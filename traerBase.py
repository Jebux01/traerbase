#!/bin/bash
import os
import sys
import sqlite3
import threading
import subprocess as spr
import pyftpdlib
import telnetlib
import pandas as pd
import arrow
import datetime
from sqlchecadas import conexion


class check:

    def __init__(self):
        self.transfer_file(DEVICE_IP, server_ip, DB_PATH, cmd='ftpput')
        #aqui creamos el servidor ftp con ftput, los checadores corren por busybox, asi que seria crear un servidor pasando datos binarios
        self.transfer_file(server_ip, DEVICE_IP, DB_PATH, cmd='ftpget')
        #renombramos los archivos para poder accederlos
        os.rename('ZKDB.db', 'ZKD' + NAMEDB + '.db')

        self.user_pin()

    #esta funcion recive 4 parametros, ip del dispositivo, ip a donde descargaremos la data, el path donde esta guardado la db y el comando que     ejecutaremos
    def transfer_file(self, from_ip, to_ip, remote_file_path, cmd='ftpput'):
        ftp_server = spr.Popen([sys.executable, '-m', 'pyftpdlib', '-w'])
        print('Server started')
        filename = os.path.basename(remote_file_path)

        #todas las conexiones a los dispositivos son por telnet
        s = telnetlib.Telnet(DEVICE_IP)
        #los usuarios y contraseñas pueden variar dependiendo del modelo del checador, el modelo del cual hice es el ZMM100
        print(s.read_until(b'login: ').decode())
        s.write(b'root \n')
        print(s.read_until(b'Password: ').decode())
        s.write(b'solokey\n')
        if s.read_until(b'#'):
            s.write(bytes('ls %s\n' % DB_PATH, 'utf-8'))
            files = s.read_until(b'#').decode()

            if filename in files:
                while True:
                    if cmd == 'ftpput':
                        command = bytes('%s -P 2121 %s %s %s\n' % (cmd, server_ip,
                                                                   filename,
                                                                   remote_file_path),
                                        'utf-8')
                    elif cmd == 'ftpget':
                        command = bytes(
                            '%s -P 2121 %s %s %s\n' % (cmd, server_ip, remote_file_path, filename), 'utf-8')
                    else:
                        raise ValueError('cmd must be `ftpput` or `ftpget`')
                    s.write(command)
                    ret = s.read_until(b'#').decode()
                    if 'refused' not in ret:
                        print(ret)
                        break

        ftp_server.kill()
        print('Server killed')

    def user_pin(self):
        try:
            con = sqlite3.connect('ZKD' + NAMEDB + '.db')

            cur = con.cursor()

            cur.execute("SELECT \
                    al.ID, \
                    ui.User_PIN, \
                    Verify_Time \
                FROM \
                    ATT_LOG al \
                JOIN USER_INFO ui ON \
                    al.User_PIN = ui.User_PIN \
                WHERE \
                    Verify_Time BETWEEN ( \
                    SELECT \
                        DATE('now', '-1 day') || 'T10:00:01') AND ( \
                    SELECT \
                        DATE() || 'T10:00:00')")

            resp = cur.fetchall()

            con.close()

            df1 = pd.DataFrame(resp)
            df1['IdTerminal'] = IDTERMINAL
            df1.columns = ["IdRegistry", "User_PIN", "Date", "IdTerminal"]

            self.insertData(df1)

        except Exception as ex:
            print(ex)

    def insertData(self, data):
        for index, row in data.iterrows():
            with conexion.cursor() as cursor:
                dateFormat = arrow.get(row['Date']).format(
                    'YYYY-MM-DD HH:mm:ss')
                # print({"convertir arrow": dateFormat})
                # date_str = datetime.datetime.strptime(str(dateFormat), '%Y-%m-%dT%H:%M:%S+%f')
                # print({"fecha parseada": date_str})
                cursor.execute("INSERT INTO dbo.TChecadas ([IdRegistry], [User_PIN], [Date], [IdTerminal]) VALUES (?,?,?,?)", int(
                    row['IdRegistry']), int(row['User_PIN']), dateFormat, row['IdTerminal'])

                cursor.commit()

        print("data ready: ", NAMEDB)


def get_server_ip(device_ip):
    import socket

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect((device_ip, 80))
    return s.getsockname()[0]


if __name__ == '__main__':
    #hacemos una conexion al servidor sql server para poder obtener las direcciones ip de los diferentes checadores
    #si no tenemos la direccion ip, con un nmap buscando todos los dispositivos que tengan el puerto 23 abierto
    with conexion.cursor() as cursor:
        consulta = "SELECT * FROM dbTerminales"
        cursor.execute(consulta)

        terminales = cursor.fetchall()

        #aqui asignamos las ip a variables estaticas
        for terminal in terminales:
            IDTERMINAL = terminal[0]
            NAMEDB = terminal[1]
            DEVICE_IP = terminal[2]
            print("get data from: ", DEVICE_IP, NAMEDB)
            #asignamos el directorio donde se guarda la db de sqlite
            DB_PATH = '/mnt/mtdblock/data/ZKDB.db'
            DB = os.path.basename(DB_PATH)
            #tomamos la direccion ip de la maquina donde ejecutamos el script
            server_ip = get_server_ip(DEVICE_IP)

            check()

